# README #

Tout ce qui est ici fait partie d'un dossier concernant tout ce qui était en rapport avec la BDD du bot dont le scraping et les scripts sql.

### Comment lire le dossier ###
le dossier ###DataSql### est le dossier dans lequel est stocké les scripts sql generé par le php
le dossier ###Img### est le dossier dans lequel est stocké les images des cartes récupéré par le php
le dossier ###Scraping_Ws### est le dossier dans lequel est codé le scrapeur avec php via une librairie "simple_html_dom"
	###ScrapeurScript.php### est le fichier php utilisé pour enregistrer tout un deck ou une suite logique de carte (par exemple toute les carte d'un meme anime)
	###ScrapeurScriptOne.php### est la juste pour enregistrer une carte en particulier
	le dossier ###images### est le dossier ou sont enregistrer temporairement les images du processus en cours
	